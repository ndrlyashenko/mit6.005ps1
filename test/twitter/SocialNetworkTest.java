/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package twitter;

import static org.junit.Assert.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class SocialNetworkTest {

    /*
     * Test strategy
     * 
     * tweets.size: 0, 1, > 1
     * 
     * tweet with mention its author should be ignored
     * tweet with mention of another user
     * tweet with no mention of another user 
     */

    private static final Instant d1 = Instant.parse("2016-02-17T10:00:00Z");
    
    private static final Tweet tweet1 = new Tweet(1, "alyssa", "@alyssa hi, bob, is it reasonable to talk about rivest so much?", d1);
    private static final Tweet tweet2 = new Tweet(2, "bob", "@alyssa hi, i'm bob", d1);
    private static final Tweet tweet3 = new Tweet(3, "tret", "hi, bob, is it reasonable to talk about rivest so much?", d1);
    private static final Tweet tweet4 = new Tweet(4, "bob", "@tret hi, i'm bob", d1);
    private static final Tweet tweet5 = new Tweet(5, "alyssa", "@tret hi, bob, is it reasonable to talk about rivest so much?", d1);
    
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void testGuessFollowsGraphEmpty() {
        final Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(new ArrayList<>());
        
        assertTrue("expected empty graph", followsGraph.isEmpty());
    } 
    
    @Test
    public void testGuessFollowsGraph() {
        final Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(Arrays.asList(tweet1, tweet2, tweet3, tweet4, tweet5));

        assertFalse("expected non-empty graph", followsGraph.isEmpty());
        assertEquals("expected list to contain follows", followsGraph.get("@bob").size(), 2);
        assertEquals("expected list to contain follows", followsGraph.get("@alyssa").size(), 1);
        assertTrue("expected list to contain follows", followsGraph.get("@tret").isEmpty());
    }
    
    @Test
    public void testInfluencersEmpty() {
        final Map<String, Set<String>> followsGraph = new HashMap<>();
        final List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue("expected empty list", influencers.isEmpty());
    }
    
    @Test
    public void testInfluencers() {
        final Map<String, Set<String>> followsGraph = new HashMap<String , Set<String>>() {{
            put("@bot1", new TreeSet(Arrays.asList("@alys", "@bob", "@mark", "@john")));
            put("@bot2", new TreeSet(Arrays.asList("@alys", "@bob", "@mark")));
            put("@bot3", new TreeSet(Arrays.asList("@alys", "@bob")));
            put("@bot4", new TreeSet(Arrays.asList("@alys")));
        }};
        final List<String> influencers = SocialNetwork.influencers(followsGraph);

        assertFalse("expected non-empty graph", influencers.isEmpty());
        assertTrue("expected list to contain influencers", influencers.containsAll(Arrays.asList("@alys", "@bob", "@mark", "@john")));
        assertEquals("expected sorted order", 0, influencers.indexOf("@alys"));
    }
    
    @Test
    public void testInfluencersCaseInsensitive() {
        final Map<String, Set<String>> followsGraph = new HashMap<String , Set<String>>() {{
            put("@bot1", new TreeSet(Arrays.asList("@alys", "@bob", "@mark", "@john")));
            put("@bot2", new TreeSet(Arrays.asList("@aLys", "@bob", "@mark")));
            put("@bot3", new TreeSet(Arrays.asList("@alYs", "@bob")));
            put("@bot4", new TreeSet(Arrays.asList("@alyS")));
        }};
        final List<String> influencers = SocialNetwork.influencers(followsGraph);

        assertFalse("expected non-empty graph", influencers.isEmpty());
        assertTrue("expected list to contain influencer", influencers.stream().anyMatch("@alys"::equalsIgnoreCase));
    }

    /*
     * Warning: all the tests you write here must be runnable against any
     * SocialNetwork class that follows the spec. It will be run against several
     * staff implementations of SocialNetwork, which will be done by overwriting
     * (temporarily) your version of SocialNetwork with the staff's version.
     * DO NOT strengthen the spec of SocialNetwork or its methods.
     * 
     * In particular, your test cases must not call helper methods of your own
     * that you have put in SocialNetwork, because that means you're testing a
     * stronger spec than SocialNetwork says. If you need such helper methods,
     * define them in a different class. If you only need them in this test
     * class, then keep them in this test class.
     */

}
